@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-5">
            <img src="{{ url('images/1.png')}}" class="rounded mx-auto d-block" width="300" alt="">
        </div>
        @foreach($barang as $barang)
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{url('gambarimg')}}/{{$barang->gambar}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">{{ $barang->nama_barang}}</h5>
                <p class="card-text">
                    <strong>Harga :</strong> Rp. {{number_format($barang->harga)}} <br>
                    <strong>Stock :</strong> {{$barang->stok}} <br>
                    <br>
                    {{$barang->keterangan}}
                </p>
                <a href="{{ url('pesan') }}/{{$barang->id}}" class="btn btn-primary" ><i class="fas fa-shopping-cart"></i> Pesan</a>
            </div>
            </div>
        </div>
        @endforeach
        
    </div>
</div>
@endsection
