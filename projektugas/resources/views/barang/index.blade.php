@extends('admin.master')

@section('judul')
    Halaman Tambah barang
@endsection

@section('content')
<div class="row">
    @forelse ($barang as $item)
    <div class ="col-4 mt-2"> 
        <div class="card" style="width: 18rem;">
            <img src="{{asset('gambarimg/'. $item->gambar)}}" class="card-img-top" height="300px" width="200px" alt="...">
            <div class="card-body">
                <h5 class="card-title" >{{$item->nama_barang}}</h5>
                <p class="card-text" >{{Str::limit($item->keterangan, 50)}}</p>
                
                <form action="/barang/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/barang/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/barang/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete " class="btn btn-danger sm">
                </form>
            </div>
        </div>
    </div> 
    @empty
        <h3>Belum ada data barang</h3>
    @endforelse
</div>
@endsection