@extends('admin.master')

@section('judul')
    EDIT DATA BARANG
@endsection

@section('content')
<form action="/barang/{{$barang->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >nama_barang</label>
      <input type="text" name="nama_barang"   value="{{$barang->nama_barang}}"class="form-control" >
    </div>
    @error('nama_barang')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >harga</label>
        <input type="text" name="harga"  value="{{$barang->harga}}" class="form-control" >
    </div>
    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >stok</label>
      <input type="INT" name="stok"  value="{{$barang->stok}}" class="form-control" >
    </div>
    @error('stok')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >gambar</label>
      <input type="file" name="gambar"  value="{{$barang->gambar}}" class="form-control" >
    </div>
    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >keterangan</label>
        <textarea name="keterangan"  value="{{$barang->keterangan}}" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection