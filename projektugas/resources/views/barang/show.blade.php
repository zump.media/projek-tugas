@extends('admin.master')

@section('judul')
    Halaman Show barang
@endsection

@section('content')
<div class="col-md-12 mt-1">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ url('gambarimg') }}/{{ $barang->gambar }}" class="rounded mx-auto d-block" width="100%" alt=""> 
                </div>
                <div class="col-md-6 mt-5">
                    <h2>{{ $barang->nama_barang }}</h2>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Harga</td>
                                <td>:</td>
                                <td>Rp. {{ number_format($barang->harga) }}</td>
                            </tr>
                            <tr>
                                <td>Stok</td>
                                <td>:</td>
                                <td>{{ number_format($barang->stok) }}</td>
                            </tr>
                            <tr>
                                <td>Keterangan</td>
                                <td>:</td>
                                <td>{{ $barang->keterangan }}</td>
                            </tr>
                           
                            
                            
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection