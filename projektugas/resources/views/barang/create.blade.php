@extends('admin.master')

@section('judul')
    TAMBAH DATA BARANG
@endsection

@section('content')
<form action="/barang" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label >nama_barang</label>
      <input type="text" name="nama_barang" class="form-control" >
    </div>
    @error('nama_barang')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >harga</label>
        <input type="text" name="harga" class="form-control" >
    </div>
    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >stok</label>
      <input type="INT" name="stok" class="form-control" >
    </div>
    @error('stok')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >gambar</label>
      <input type="file" name="gambar" class="form-control" >
    </div>
    @error('gambar')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >keterangan</label>
        <textarea name="keterangan" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection