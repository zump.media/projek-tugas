<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{

	protected $table = 'barangs';
    
    protected $fillable = ['nama_barang','harga','stok','gambar','keterangan'];

    public function pesanan_detail() 
	{
	     return $this->hasMany('App\pesanandetail','barang_id', 'id');
	}

}
